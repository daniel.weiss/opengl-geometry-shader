#pragma once

#include "scene.h"
#include "glslprogram.h"

#include "cookbookogl.h"
#include "camera/OrbitCamera.h"

#include <memory>

class ScenePointToCube : public Scene
{
private:
    GLSLProgram prog;

    GLuint sprites;
    int numSprites;
    float *locations;
    float size = 0.15f;
    float rotation = 3.0f;

    std::unique_ptr<Camera> m_camera;

    void setMatrices();
    void compileAndLinkShader();
    void setupFBO();

public:
    ScenePointToCube();

    void initScene();
    void update( float t );
    void render();
    void resize(int, int);

    int renderGUI() override;
};
