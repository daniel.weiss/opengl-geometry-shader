#pragma once

#include "scene.h"
#include "glslprogram.h"
#include "objmesh.h"
#include "camera/FirstPersonCamera.h"

#include "cookbookogl.h"

#include <glm/glm.hpp>
#include <GLFW/glfw3.h>

class SceneNormals : public Scene
{
private:
    GLSLProgram prog;
    GLSLProgram debug;

    FirstPersonCamera camera;

    std::unique_ptr<ObjMesh> geometry;
    float angle, tPrev, rotSpeed;

    void setMatrices();
    void compileAndLinkShader();
    void setupFBO();

    glm::vec3 cameraPos;

public:
    SceneNormals();

    void initScene();
    void update(float t);
    void render();
    void resize(int, int);

    void acceptUserInput(int, int, int) override;
};