#pragma once

#include "Camera.h"

class OrbitCamera : public Camera
{
public:
	OrbitCamera(
		float radius = 2.0f,
		float speed = 0.1f,
		glm::vec3 center = glm::vec3(0.0f, 0.0f, 0.0f),
		glm::vec3 upVector = glm::vec3(0.0f, 1.0f, 0.0f)
				) : 
		radius(radius), speed(speed), center(center), upVector(upVector)
	{
		viewMatrix = glm::lookAt(glm::vec3(0.0f, 0.0f, 2.0f), center, upVector);
	}

	void updateViewMatrix(float time)
	{
		float camX = sin(time * speed) * radius;
		float camZ = cos(time * speed) * radius;
		viewMatrix = glm::lookAt(glm::vec3(camX, 0.0, camZ), center, upVector);
	}

	glm::mat4 getViewMatrix() override
	{
		return viewMatrix;
	}

	void changeSpeed(float speed)
	{
		this->speed = speed;
	}

	void changeRadius(float radius)
	{
		this->radius = radius;
	}

private:
	// position gets calculated in function
	glm::vec3 center;
	glm::vec3 upVector;

	glm::mat4 viewMatrix;

	float radius;
	float speed;

};