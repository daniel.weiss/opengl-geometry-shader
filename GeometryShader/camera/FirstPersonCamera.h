#pragma once

#include "Camera.h"

#include <iostream>

class FirstPersonCamera : public Camera
{
public:
	FirstPersonCamera(float speed = 0.1f, 
					glm::vec3 position = glm::vec3(0.0f, 0.0f, 2.0f),
					glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f),
					glm::vec3 upVector = glm::vec3(0.0f, 1.0f, 0.0f)) 
		: SPEED(speed), position(position), cameraFront(cameraFront), upVector(upVector)
	{
		cameraFront.x = cos(glm::radians(YAW)) * cos(glm::radians(PITCH));
		cameraFront.y = sin(glm::radians(PITCH));
		cameraFront.z = sin(glm::radians(YAW)) * cos(glm::radians(PITCH));

		cameraFront = glm::normalize(cameraFront);

		// recalculate right vector for keyboard input
		rightVector = glm::normalize(glm::cross(cameraFront, upVector));

		viewMatrix = glm::lookAt(position, position + cameraFront, upVector);
		rightVector = glm::normalize(glm::cross(cameraFront, upVector));
	}

	void updateViewMatrix(float) override
	{

	}
	glm::mat4 getViewMatrix() override
	{
		return viewMatrix;
	}

	void updateCameraFromMouseInput(int delta_x, int delta_y) override 
	{
		float x = delta_x * SPEED;
		float y = - delta_y * SPEED;

		YAW += x;
		PITCH += y;

		if (PITCH > 89.0f)
			PITCH = 89.0f;
		if (PITCH < -89.0f)
			PITCH = -89.0f;

		
		cameraFront.x = cos(glm::radians(YAW)) * cos(glm::radians(PITCH));
		cameraFront.y = sin(glm::radians(PITCH));
		cameraFront.z = sin(glm::radians(YAW)) * cos(glm::radians(PITCH));

		cameraFront = glm::normalize(cameraFront);

		// recalculate right vector for keyboard input
		rightVector = glm::normalize(glm::cross(cameraFront, upVector));

		viewMatrix = glm::lookAt(position, position + cameraFront, upVector);
	}
	void updateCameraFromKeryboardInput(int key) override 
	{
		if (key == GLFW_KEY_W)
		{
			// move forward
			position += cameraFront * SPEED;

		}
		if (key == GLFW_KEY_A)
		{
			// move to the left
			position += -rightVector * SPEED;
		}
		if (key == GLFW_KEY_S)
		{
			// move backwards
			position += -cameraFront * SPEED;
		}
		if (key == GLFW_KEY_D)
		{
			// move to the right
			position += rightVector * SPEED;
		}
		if (key == GLFW_KEY_SPACE)
		{
			// move camera up
			position += upVector * SPEED;
		}
		if (key == GLFW_KEY_LEFT_CONTROL)
		{
			// move camera down
			position += - upVector * SPEED;
		}

		viewMatrix = glm::lookAt(position, position + cameraFront, upVector);
	}

private:
	float PITCH =	0.f;
	float YAW =		-90.0f;
	float ROLL =	0.f;

	float SPEED = 0.1f;

	glm::vec3 position;
	glm::vec3 cameraFront;
	glm::vec3 upVector;
	glm::vec3 rightVector;

	glm::mat4 viewMatrix; 
};