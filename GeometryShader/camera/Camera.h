#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GLFW/glfw3.h>

class Camera
{
public:
	virtual glm::mat4 getViewMatrix() = 0;
	virtual void updateViewMatrix(float) = 0;

	virtual void updateCameraFromMouseInput(int, int) {}
	virtual void updateCameraFromKeryboardInput(int) {}
};