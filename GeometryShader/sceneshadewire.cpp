#include "sceneshadewire.h"

#include <iostream>
using std::cerr;
using std::endl;

#include <glm/gtc/matrix_transform.hpp>
using glm::vec3;
using glm::vec4;
using glm::mat4;

SceneShadeWire::SceneShadeWire() {
    ogre = ObjMesh::load("../models/bs_ears.obj");
    camera = std::unique_ptr<Camera>(new OrbitCamera(2.0f));
}

void SceneShadeWire::initScene()
{
    compileAndLinkShader();
    glClearColor(0.5f,0.5f,0.5f,1.0f);

    glEnable(GL_DEPTH_TEST);

    float c = 1.5f;
    projection = glm::ortho(-0.4f * c, 0.4f * c, -0.3f *c, 0.3f*c, 0.1f, 100.0f);

	///////////// Uniforms ////////////////////
    prog.setUniform("Line.Width", 0.75f);
	prog.setUniform("Line.Color", vec4(0.05f,0.0f,0.05f,1.0f));
	prog.setUniform("Material.Kd", 0.7f, 0.7f, 0.7f);
	prog.setUniform("Light.Position", vec4(0.0f,0.0f,0.0f, 1.0f));
	prog.setUniform("Material.Ka", 0.2f, 0.2f, 0.2f);
	prog.setUniform("Light.Intensity", 1.0f, 1.0f, 1.0f);
	prog.setUniform("Material.Ks", 0.8f, 0.8f, 0.8f);
	prog.setUniform("Material.Shininess", 100.0f);
	/////////////////////////////////////////////
}


void SceneShadeWire::update( float t ) 
{
    if (animating())
        camera->updateViewMatrix(t);
}

void SceneShadeWire::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    vec3 cameraPos(0.0f, 0.0f, 3.0f);
    //view = glm::lookAt(cameraPos,
    //                   vec3(0.0f,0.0f,0.0f),
    //                   vec3(0.0f,1.0f,0.0f));

    view = camera->getViewMatrix();

    model = mat4(1.0f);
    setMatrices();
    ogre->render();

    glFinish();
}

int SceneShadeWire::renderGUI()
{
    int ret = 0;
    //Start the Dear ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();

    ImGui::NewFrame();
    {
        ImGui::Begin("Navigation");

        if (ImGui::Button("Back", ImVec2(130, 30)))
        {
            ret = -1;
        }
        ImGui::Checkbox("Animating", &m_animate);

        if (ImGui::SliderFloat("Speed", &cameraSpeed, 0.0f, 1.0f))
            ((OrbitCamera*)camera.get())->changeSpeed(cameraSpeed);

        ImGui::Text("Application average\n%.3f ms/frame \n(%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

        ImGui::End();
    }

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    return ret;
}

void SceneShadeWire::setMatrices()
{
    mat4 mv = view * model;
    prog.setUniform("ModelViewMatrix", mv);
    prog.setUniform("NormalMatrix",
                    glm::mat3( vec3(mv[0]), vec3(mv[1]), vec3(mv[2]) ));
    prog.setUniform("MVP", projection * mv);
    prog.setUniform("ViewportMatrix", viewport);
}

void SceneShadeWire::resize(int w, int h)
{
    glViewport(0,0,w,h);

    float w2 = w / 2.0f;
    float h2 = h / 2.0f;
    viewport = mat4( vec4(w2,0.0f,0.0f,0.0f),
                     vec4(0.0f,h2,0.0f,0.0f),
                     vec4(0.0f,0.0f,1.0f,0.0f),
                     vec4(w2+0, h2+0, 0.0f, 1.0f));

    projection = glm::perspective(glm::radians(35.0f), (float)w / h, 0.3f, 100.0f);

}

void SceneShadeWire::acceptUserInput(int mode, int x, int y)
{
}

void SceneShadeWire::compileAndLinkShader()
{
	try {
		prog.compileShader("shader/shadewire.vert",GLSLShader::VERTEX);
		prog.compileShader("shader/shadewire.frag",GLSLShader::FRAGMENT);
		prog.compileShader("shader/shadewire.geom",GLSLShader::GEOMETRY);
		prog.link();
    	prog.use();
    } catch(GLSLProgramException &e ) {
    	cerr << e.what() << endl;
 		exit( EXIT_FAILURE );
    }
}
