#pragma once

#include "scene.h"
#include "glslprogram.h"
#include "objmesh.h"

#include "cookbookogl.h"

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

class SceneExplosion : public Scene
{
private:
    GLSLProgram prog;

    std::unique_ptr<ObjMesh> ogre;
    float angle, tPrev, rotSpeed;
    bool triggeredExplosion = false;

    void setMatrices();
    void compileAndLinkShader();
    void setupFBO();

public:
    SceneExplosion();

    void initScene();
    void update(float t);
    void render();
    void resize(int, int);

    void acceptUserInput(int mode, int x = 0, int y = 0) override;
    int renderGUI() override;
};
