#include "scenePointToCube.h"
#include "texture.h"

#include <cstdlib>
using std::rand;
using std::srand;

#include <ctime>
using std::time;

#include <iostream>
using std::cerr;
using std::endl;

#include <glm/gtc/matrix_transform.hpp>
using glm::vec3;
using glm::mat4;

ScenePointToCube::ScenePointToCube() 
{
    m_camera = std::unique_ptr<Camera>(new OrbitCamera());
}

void ScenePointToCube::initScene()
{

    compileAndLinkShader();

    glClearColor(0.5f,0.5f,0.5f,1.0f);

    glEnable(GL_DEPTH_TEST);

    numSprites = 50;
    locations = new float[numSprites * 3];
    srand( (unsigned int)time(0) );
    for( int i = 0; i < numSprites; i++ ) {
        vec3 p(((float)rand() / RAND_MAX * 2.0f) - 1.0f,
               ((float)rand() / RAND_MAX * 2.0f) - 1.0f,
               ((float)rand() / RAND_MAX * 2.0f) - 1.0f);
        locations[i*3] = p.x;
        locations[i*3+1] = p.y;
        locations[i*3+2] = p.z;
    }

    // Set up the buffers
    GLuint handle;
    glGenBuffers(1, &handle);

    glBindBuffer(GL_ARRAY_BUFFER, handle);
    glBufferData(GL_ARRAY_BUFFER, numSprites * 3 * sizeof(float), locations, GL_STATIC_DRAW);

    delete [] locations;

    // Set up the vertex array object
    glGenVertexArrays( 1, &sprites );
    glBindVertexArray(sprites);

    glBindBuffer(GL_ARRAY_BUFFER, handle);
    glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, ((GLubyte *)NULL + (0)) );
    glEnableVertexAttribArray(0);  // Vertex position

    glBindVertexArray(0);

    // Load texture file
    const char * texName = "../textures/flower.png";
    Texture::loadTexture(texName);

    prog.setUniform("SpriteTex", 0);
    prog.setUniform("Size2", size);

    model = mat4(1.0f);
    glm::mat4 rotMat = glm::rotate(glm::mat4(1.0f), rotation, glm::vec3(0.0f, 1.0f, 0.0f));
    prog.setUniform("Rotation", rotMat);
}


void ScenePointToCube::update( float t ) 
{
    if (animating())
        m_camera->updateViewMatrix(t);

    //model = glm::rotate(model, 0.01f, glm::vec3(0.0f, 1.0f, 0.0f));
}

void ScenePointToCube::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //vec3 cameraPos(0.0f,0.0f,3.0f);
    //view = glm::lookAt(cameraPos,
    //                   vec3(0.0f,0.0f,0.0f),
    //                   vec3(0.0f,1.0f,0.0f));

    view = m_camera->getViewMatrix();

    setMatrices();

    glBindVertexArray(sprites);
    glDrawArrays(GL_POINTS, 0, numSprites);

    glFinish();
}

int ScenePointToCube::renderGUI()
{
    int ret = 0;
    //Start the Dear ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();

    ImGui::NewFrame();
    {
        ImGui::Begin("Navigation");

        if (ImGui::Button("Back", ImVec2(130, 30)))
        {
            ret = -1;
        }
        ImGui::Checkbox("Animating", &m_animate);

        if (ImGui::SliderFloat("Size", &size, 0.0f, 1.0f))
            prog.setUniform("Size2", size);

        if (ImGui::SliderFloat("Rotation", &rotation, 0.0f, 7.0f))
        {
            glm::mat4 rotMat =  glm::rotate(glm::mat4(1.0f), rotation, glm::vec3(0.0f, 1.0f, 0.0f));
            prog.setUniform("Rotation", rotMat);

        }
            
        ImGui::Text("Application average\n%.3f ms/frame \n(%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

        ImGui::End();
    }

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    return ret;
}

void ScenePointToCube::setMatrices()
{
    mat4 mv = view * model;
    prog.setUniform("ModelViewMatrix", mv);
    prog.setUniform("ProjectionMatrix", projection);
}

void ScenePointToCube::resize(int w, int h)
{
    glViewport(0,0,w,h);
    projection = glm::perspective(glm::radians(60.0f), (float)w/h, 0.3f, 100.0f);
}

void ScenePointToCube::compileAndLinkShader()
{
	try {
		prog.compileShader("shader/pointToCube.vert");
		prog.compileShader("shader/pointToCube.frag");
		prog.compileShader("shader/pointToCube.geom");
    	prog.link();
    	prog.use();
    } catch(GLSLProgramException &e ) {
    	cerr << e.what() << endl;
 		exit( EXIT_FAILURE );
    }
}
