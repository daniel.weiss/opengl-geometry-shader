#ifndef SCENESHADEWIRE_H
#define SCENESHADEWIRE_H

#include "scene.h"
#include "glslprogram.h"
#include "objmesh.h"

#include "cookbookogl.h"

#include <glm/glm.hpp>

#include "camera/OrbitCamera.h"

class SceneShadeWire : public Scene
{
private:
    GLSLProgram prog;

    std::unique_ptr<ObjMesh> ogre;

    glm::mat4 viewport;

    std::unique_ptr<Camera> camera;

    float cameraSpeed = 1.0f;

    void setMatrices();
    void compileAndLinkShader();
    void setupFBO();

public:
    SceneShadeWire();

    void initScene();
    void update( float t );
    void render();
    void resize(int, int);
    void acceptUserInput(int, int, int) override;
    int renderGUI() override;
};

#endif // SCENESHADEWIRE_H
