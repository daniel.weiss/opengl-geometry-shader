#include "sceneexplosion.h"

#include <iostream>
using std::cerr;
using std::endl;

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>
using glm::vec3;
using glm::mat4;
using glm::vec4;
using glm::mat3;

SceneExplosion::SceneExplosion() : angle(0.0f), tPrev(0.0f), rotSpeed(glm::pi<float>() / 8.0f) {
    ogre = ObjMesh::loadWithAdjacency("../models/bs_ears.obj");
}

void SceneExplosion::initScene()
{
    compileAndLinkShader();

    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

    glEnable(GL_DEPTH_TEST);

    angle = glm::half_pi<float>();

    ///////////// Uniforms ////////////////////
    prog.setUniform("EdgeWidth", 0.015f);
    prog.setUniform("PctExtend", 0.25f);
    prog.setUniform("LineColor", vec4(0.05f, 0.0f, 0.05f, 1.0f));
    prog.setUniform("Material.Kd", 0.7f, 0.5f, 0.2f);
    prog.setUniform("Light.Position", vec4(0.0f, 0.0f, 0.0f, 1.0f));
    prog.setUniform("Material.Ka", 0.2f, 0.2f, 0.2f);
    prog.setUniform("Light.Intensity", 1.0f, 1.0f, 1.0f);
    prog.setUniform("dist", 0.0f);
    /////////////////////////////////////////////
}

namespace
{
    float dist = 0.0;
};


void SceneExplosion::update(float t)
{
    float deltaT = t - tPrev;
    if (tPrev == 0.0f) deltaT = 0.0f;
    tPrev = t;

    prog.setUniform("time", t);

    if (triggeredExplosion)
    {
        dist += 0.01;
        prog.setUniform("dist", dist);
    }
}

void SceneExplosion::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //vec3 cameraPos(1.5f * cos(angle), 0.0f, 1.5f * sin(angle));
    vec3 cameraPos(0.0f, 0.0f, 4.5f);
    view = glm::lookAt(cameraPos,
        vec3(0.0f, -0.2f, 0.0f),
        vec3(0.0f, 1.0f, 0.0f));

    model = mat4(1.0f);
    setMatrices();
    ogre->render();

    glFinish();
}

int SceneExplosion::renderGUI() {

    int ret = 0;
    //Start the Dear ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();

    ImGui::NewFrame();
    {
        ImGui::Begin("Navigation");

        if (ImGui::Button("Back", ImVec2(130, 30)))
        {
            ret = -1;
        }
        if (ImGui::Button("Explode", ImVec2(130, 30)))
        {
            std::cout << "Triggering Explosion" << std::endl;
            triggeredExplosion = true;
            prog.setUniform("exploding", true);
        }
        if (ImGui::Button("Reset", ImVec2(130, 30)))
        {
            std::cout << "Resetting explosion state" << std::endl;
            triggeredExplosion = false;
            prog.setUniform("exploding", false);
            dist = 0.0;
        }

        ImGui::Text("Application average\n%.3f ms/frame \n(%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

        ImGui::End();
    }

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    return ret;
};

void SceneExplosion::setMatrices()
{
    mat4 mv = view * model;
    prog.setUniform("ModelViewMatrix", mv);
    prog.setUniform("NormalMatrix",
        mat3(vec3(mv[0]), vec3(mv[1]), vec3(mv[2])));
    prog.setUniform("MVP", projection * mv);
}

void SceneExplosion::resize(int w, int h)
{
    glViewport(0, 0, w, h);
    float c = 1.5f;
    //projection = glm::ortho(-0.4f * c, 0.4f * c, -0.3f * c, 0.3f * c, 0.1f, 100.0f);
    projection = glm::perspective(glm::radians(35.0f), (float)w / h, 0.3f, 100.0f);
}

void SceneExplosion::compileAndLinkShader()
{
    try {
        prog.compileShader("shader/ogre.vert", GLSLShader::VERTEX);
        prog.compileShader("shader/ogre.frag", GLSLShader::FRAGMENT);
        prog.compileShader("shader/explosion.geom", GLSLShader::GEOMETRY);
        prog.link();
        prog.use();
    }
    catch (GLSLProgramException& e) {
        cerr << e.what() << endl;
        exit(EXIT_FAILURE);
    }
}

void SceneExplosion::acceptUserInput(int mode, int x, int y)
{
}
