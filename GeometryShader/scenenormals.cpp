#include "scenenormals.h"

#include <iostream>
using std::cerr;
using std::endl;

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/constants.hpp>
using glm::vec3;
using glm::mat4;
using glm::vec4;
using glm::mat3;

SceneNormals::SceneNormals() : angle(0.0f), tPrev(0.0f), rotSpeed(glm::pi<float>() / 8.0f) {
    //ogre = ObjMesh::loadWithAdjacency("../models/bs_ears.obj");
    geometry = ObjMesh::loadWithAdjacency("./models/sphere.obj");
    camera = FirstPersonCamera(0.1f);
}

void SceneNormals::initScene()
{
    compileAndLinkShader();

    glClearColor(0.5f, 0.5f, 0.5f, 1.0f);

    glEnable(GL_DEPTH_TEST);

    angle = glm::half_pi<float>();

    ///////////// Uniforms ////////////////////
    prog.use();
    prog.setUniform("EdgeWidth", 0.015f);
    prog.setUniform("PctExtend", 0.25f);
    prog.setUniform("LineColor", vec4(0.05f, 0.0f, 0.05f, 1.0f));
    prog.setUniform("Material.Kd", 0.7f, 0.5f, 0.2f);
    prog.setUniform("Light.Position", vec4(0.0f, 0.0f, 0.0f, 1.0f));
    prog.setUniform("Material.Ka", 0.2f, 0.2f, 0.2f);
    prog.setUniform("Light.Intensity", 1.0f, 1.0f, 1.0f);
    /////////////////////////////////////////////

    ///////////// Uniforms ////////////////////
    debug.use();
    debug.setUniform("EdgeWidth", 0.015f);
    debug.setUniform("PctExtend", 0.25f);
    debug.setUniform("LineColor", vec4(0.05f, 0.0f, 0.05f, 1.0f));
    debug.setUniform("Material.Kd", 0.7f, 0.5f, 0.2f);
    debug.setUniform("Light.Position", vec4(0.0f, 0.0f, 0.0f, 1.0f));
    debug.setUniform("Material.Ka", 0.2f, 0.2f, 0.2f);
    debug.setUniform("Light.Intensity", 1.0f, 1.0f, 1.0f);
    /////////////////////////////////////////////

    /// Camera ///
    //cameraPos = vec3(0.0f, 0.5f, 10.0f);
    //view = glm::lookAt(cameraPos,
    //    vec3(0.0f, 0.5f, 0.0f),
    //    vec3(0.0f, 1.0f, 0.0f));
    view = camera.getViewMatrix();

}


void SceneNormals::update(float t)
{
    //float deltaT = t - tPrev;
    //if (tPrev == 0.0f) deltaT = 0.0f;
    //tPrev = t;

    //angle += rotSpeed * deltaT;
    //if (angle > glm::two_pi<float>()) angle -= glm::two_pi<float>();
}

void SceneNormals::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //vec3 cameraPos(1.5f * cos(angle), 0.0f, 1.5f * sin(angle));
    //view = glm::lookAt(cameraPos,
    //    vec3(0.0f, -0.2f, 0.0f),
    //    vec3(0.0f, 1.0f, 0.0f));

    //vec3 cameraPos(1.5f * cos(angle), 0.0f, 1.5f * sin(angle));
    //view = glm::lookAt(cameraPos,
    //    vec3(0.0f, -0.2f, 0.0f),
    //    vec3(0.0f, 1.0f, 0.0f));

    view = camera.getViewMatrix();

    model = mat4(1.0f);
    setMatrices();

    debug.use();
    geometry->render();

    prog.use();
    geometry->render();

    glFinish();
}

void SceneNormals::setMatrices()
{
    mat4 mv = view * model;

    prog.use();
    prog.setUniform("ModelViewMatrix", mv);
    prog.setUniform("NormalMatrix",
        mat3(vec3(mv[0]), vec3(mv[1]), vec3(mv[2])));
    prog.setUniform("MVP", projection * mv);

    debug.use();
    debug.setUniform("ModelViewMatrix", mv);
    debug.setUniform("NormalMatrix",
        mat3(vec3(mv[0]), vec3(mv[1]), vec3(mv[2])));
    debug.setUniform("MVP", projection * mv);


}

void SceneNormals::resize(int w, int h)
{
    glViewport(0, 0, w, h);
    float c = 1.5f;
    //projection = glm::ortho(-0.4f * c, 0.4f * c, -0.3f * c, 0.3f * c, 0.1f, 100.0f);
    projection = glm::perspective(glm::radians(35.0f), (float)w / h, 0.3f, 100.0f);
}

void SceneNormals::acceptUserInput(int mode, int x, int y)
{
    camera.updateCameraFromKeryboardInput(mode);

    camera.updateCameraFromMouseInput(x, y);
}

void SceneNormals::compileAndLinkShader()
{
    try {
        prog.compileShader("shader/ogre.vert", GLSLShader::VERTEX);
        prog.compileShader("shader/ogre.frag", GLSLShader::FRAGMENT);

        debug.compileShader("shader/normals.vert", GLSLShader::VERTEX);
        debug.compileShader("shader/normals.frag", GLSLShader::FRAGMENT);
        debug.compileShader("shader/normals.geom", GLSLShader::GEOMETRY);

        prog.link();
        debug.link();

    }
    catch (GLSLProgramException& e) {
        cerr << e.what() << endl;
        exit(EXIT_FAILURE);
    }
}