#pragma once

#include "scene.h"
#include "glslprogram.h"
#include "objmesh.h"

#include "cookbookogl.h"

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "camera/FirstPersonCamera.h"

class SceneSphere : public Scene
{
private:
    GLSLProgram prog_toPoints;
    GLSLProgram prog_passThrough;

    std::unique_ptr<ObjMesh> sphere;

    glm::mat4 viewport;

    bool points = false;

    int pointSize = 5;

    std::unique_ptr<Camera> camera;

    void setMatrices();
    void compileAndLinkShader();
    void setupFBO();

public:
    SceneSphere();

    void initScene();
    void update( float t );
    void render();
    void resize(int, int);

    void acceptUserInput(int, int, int) override;
    int renderGUI() override;
};

