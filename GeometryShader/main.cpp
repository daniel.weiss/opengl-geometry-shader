#include "scenerunner.h"

#include <map>

#define VORTRAG

std::map<std::string, std::string> sceneInfo = {
            { "point-sprite", "Point sprites with the geometry shader" },
            { "shade-wire", "Uses the geometry shader to draw a mesh over a shaded object" },
            { "silhouette", "Uses the geometry shader to draw silhouette edges" },
            { "normals", "Use the geometry shader to draw normals onto a model" },
            { "explosion", "Use the geometry shader to explode a model!" },
            { "sphere", "Use the geometry shader to manipulate a sphere" },
            { "point-to-cube", "Use the geometry shader to turn points to cubes" },
};

int main(int argc, char *argv[])
{
	std::string recipe = SceneRunner::parseCLArgs(argc, argv, sceneInfo);

	SceneRunner runner("Chapter 7 - " + recipe);

	std::unique_ptr<Scene> scene;
    if( recipe == "point-sprite") 
    {
        scene = std::unique_ptr<Scene>( new ScenePointSprite() );
    } else if( recipe == "shade-wire" ) 
    {
        scene = std::unique_ptr<Scene>( new SceneShadeWire() );
    } else if( recipe == "silhouette") 
    {
        scene = std::unique_ptr<Scene>( new SceneSilhouette() );
    }
    else if (recipe == "normals") 
    {
        scene = std::unique_ptr<Scene>(new SceneNormals());
    }
    else if (recipe == "explosion")
    {
        scene = std::unique_ptr<Scene>(new SceneExplosion());
    }
    else if (recipe == "sphere")
    {
        scene = std::unique_ptr<Scene>(new SceneSphere());
    }
    else if (recipe == "point-to-cube")
    {
        scene = std::unique_ptr<Scene>(new ScenePointToCube());
    }
    else {
        printf("Unknown recipe: %s\n", recipe.c_str());
        exit(EXIT_FAILURE);
    }

#ifdef VORTRAG
    return runner.run();
#else
    return runner.run(std::move(scene));
#endif
    
}
