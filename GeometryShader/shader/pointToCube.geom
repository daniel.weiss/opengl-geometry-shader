#version 400

layout( points ) in;
layout( triangle_strip, max_vertices = 24) out;

uniform float Size2;   // Half the width of the quad

uniform mat4 ProjectionMatrix;
uniform mat4 Rotation;

out vec2 TexCoord;

void main()
{
    mat4 m = ProjectionMatrix;

    // front face 
    gl_Position = m * ( gl_in[0].gl_Position + Rotation * vec4(-Size2,-Size2,0.0,0.0));
    TexCoord = vec2(0.0,0.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(Size2,-Size2,0.0,0.0));
    TexCoord = vec2(1.0,0.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(-Size2,Size2,0.0,0.0));
    TexCoord = vec2(0.0,1.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(Size2,Size2,0.0,0.0));
    TexCoord = vec2(1.0,1.0);
    EmitVertex();
     
    // right face ! hier sind tex coords noch falsch! 
    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(Size2,-Size2,-Size2*2.0,0.0));
    TexCoord = vec2(0.0, 0.0);
    EmitVertex();
        
    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(Size2,-Size2,0.0,0.0));
    TexCoord = vec2(1.0,0.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(Size2,Size2,-Size2*2,0.0));
    TexCoord = vec2(0.0,1.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(Size2,Size2,0.0,0.0));
    TexCoord = vec2(1.0,1.0);
    EmitVertex();

    // left face 

    gl_Position = m * ( gl_in[0].gl_Position + Rotation * vec4(-Size2,-Size2,0.0,0.0));
    TexCoord = vec2(0.0,0.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(-Size2,-Size2,-Size2*2.0,0.0));
    TexCoord = vec2(1.0,0.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(-Size2,Size2,0.0,0.0));
    TexCoord = vec2(0.0,1.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(-Size2,Size2,-Size2*2,0.0));
    TexCoord = vec2(1.0,1.0);
    EmitVertex();

    // bottom face
    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(-Size2,-Size2,0.0,0.0));
    TexCoord = vec2(0.0,0.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(Size2,-Size2,0.0,0.0));
    TexCoord = vec2(1.0,0.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(-Size2,-Size2,-Size2*2,0.0));
    TexCoord = vec2(0.0,1.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(Size2,-Size2,-Size2 * 2,0.0));
    TexCoord = vec2(1.0,1.0);
    EmitVertex();

        // back face
    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(-Size2,-Size2,-Size2*2,0.0));
    TexCoord = vec2(0.0,0.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(Size2,-Size2,-Size2*2,0.0));
    TexCoord = vec2(1.0,0.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(-Size2,Size2,-Size2*2,0.0));
    TexCoord = vec2(0.0,1.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(Size2,Size2,-Size2 * 2,0.0));
    TexCoord = vec2(1.0,1.0);
    EmitVertex();

        // top face
    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(-Size2,Size2,0.0,0.0));
    TexCoord = vec2(0.0,0.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(Size2,Size2,0.0,0.0));
    TexCoord = vec2(1.0,0.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(-Size2,Size2,-Size2*2,0.0));
    TexCoord = vec2(0.0,1.0);
    EmitVertex();

    gl_Position = m * (gl_in[0].gl_Position + Rotation * vec4(Size2,Size2,-Size2 * 2,0.0));
    TexCoord = vec2(1.0,1.0);
    EmitVertex();

    EndPrimitive();
}
