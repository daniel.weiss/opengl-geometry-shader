#version 400

layout (location = 0 ) in vec3 VertexPosition;
layout (location = 1 ) in vec3 VertexNormal;

out vec3 VNormal;

uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix;
uniform mat4 ProjectionMatrix;
uniform mat4 MVP;

void main()
{
    VNormal = normalize( NormalMatrix * VertexNormal);
    gl_Position = MVP * vec4(VertexPosition,1.0);

//    mat3 normalMatrix = mat3(transpose(inverse(view * model)));
//    vs_out.normal = normalize(vec3(vec4(normalMatrix * aNormal, 0.0)));
}

