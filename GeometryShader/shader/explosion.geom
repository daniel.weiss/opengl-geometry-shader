#version 410 core
 
layout (triangles_adjacency) in;
layout (triangle_strip, max_vertices = 3) out;

in vec3 VNormal[];
in vec3 VPosition[];

out vec3 GNormal;
out vec3 GPosition;

uniform bool exploding = false;
uniform float time;
uniform float dist;

vec3 GetNormal()
{
   vec3 a = vec3(gl_in[0].gl_Position) - vec3(gl_in[1].gl_Position);
   vec3 b = vec3(gl_in[2].gl_Position) - vec3(gl_in[1].gl_Position);
   return normalize(cross(a, b));
}  

vec4 explode(vec4 position, vec3 normal)
{
    
    float magnitude = 10;
    //vec3 direction = normal * ((sin(time) + 1.0) / 2.0) * magnitude; // * dist; 
    vec3 direction = magnitude * (normal * (1.0 / (1.0 + pow(2.5, -dist)) - 0.5)); //[x steigt von 0 bis 1.0 etwa]
    return position + vec4(direction, 0.0);
} 

void generateVertex(int index)
{
    GNormal = VNormal[index];
    GPosition = VPosition[index];
    gl_Position = gl_in[index].gl_Position;
    EmitVertex();
}

void generateExplodingVertex(int index, vec3 normal)
{
    GNormal = VNormal[index];
    GPosition = VPosition[index];
    gl_Position = explode(gl_in[index].gl_Position, normal);
    EmitVertex();
}
 
void main()
{

  if (!exploding)
  {
    // pass-through geometry shader
    generateVertex(0);
    generateVertex(2);
    generateVertex(4);
  }
  else
  {
    // explode our mesh! 
    vec3 normal = GetNormal();
    generateExplodingVertex(0, normal);
    generateExplodingVertex(2, normal);
    generateExplodingVertex(4, normal);

  }
  EndPrimitive();
}
