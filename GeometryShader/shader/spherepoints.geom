#version 400

layout( triangles ) in;
layout( points, max_vertices = 3 ) out;

out vec3 GNormal;
out vec3 GPosition;

in vec3 VNormal[];
in vec3 VPosition[];

uniform mat4 ViewportMatrix;  // Viewport matrix
uniform float time; 

vec3 GetNormal()
{
   vec3 a = vec3(gl_in[0].gl_Position) - vec3(gl_in[1].gl_Position);
   vec3 b = vec3(gl_in[2].gl_Position) - vec3(gl_in[1].gl_Position);
   return normalize(cross(a, b));
}  

vec4 expand(vec4 position, vec3 normal)
{
    
    float magnitude = 2;
    vec3 direction = normal * ((sin(time) + 1.0) / 2.0) * magnitude;
    return position + vec4(direction, 0.0);
} 

void main()
{
    GNormal = - GetNormal(); //VNormal[0];
    GPosition = VPosition[0];
    gl_Position = gl_in[0].gl_Position;
    //gl_Position = expand(gl_in[0].gl_Position, GetNormal());
    EmitVertex();

    GNormal = - GetNormal(); //VNormal[1];
    GPosition = VPosition[1];
    gl_Position = gl_in[1].gl_Position;
    //gl_Position = expand(gl_in[1].gl_Position, GetNormal());
    EmitVertex();

    GNormal = - GetNormal(); //VNormal[2];
    GPosition = VPosition[2];
    gl_Position = gl_in[2].gl_Position;
    //gl_Position = expand(gl_in[2].gl_Position, GetNormal());
    EmitVertex();

    EndPrimitive();
}
