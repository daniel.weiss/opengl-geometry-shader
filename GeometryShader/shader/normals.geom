#version 400
layout (triangles_adjacency) in;
layout (line_strip, max_vertices = 6) out; // war davor 6

in vec3 VNormal[];

const float MAGNITUDE = 0.4;
 
uniform mat4 ProjectionMatrix;

void GenerateLine(int index)
{
    gl_Position = gl_in[index].gl_Position;
    EmitVertex();
    gl_Position = (gl_in[index].gl_Position - 
                                vec4(VNormal[index], 0.0) * MAGNITUDE);
    EmitVertex();
    EndPrimitive();
}

void main()
{
    GenerateLine(0); // first vertex normal
    GenerateLine(1); // second vertex normal
    GenerateLine(2); // third vertex normal

}  