#pragma once

#include "scene.h"

class SwapScene : public Scene
{
public: 
	SwapScene()
	{

	}

    void initScene() override
    {

    }

    virtual void update(float t) override
    {

    }

    virtual void render() override
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    virtual void resize(int, int) override
    {

    }

    int renderGUI() override 
    {

        int ret = 0;

        //Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();

        ImGui::NewFrame();
        {
            ImGui::Begin("Navigation");  // Create a window called "" and append into it.

            if (ImGui::Button("Sphere", ImVec2(130, 30)))
            {
                ret = 1;
            }
            if (ImGui::Button("Points-to-Cubes", ImVec2(130, 30)))
            {
                ret = 2;
            }
            if (ImGui::Button("Points-to-Sprites", ImVec2(130, 30)))
            {
                ret = 3;
            }
            if (ImGui::Button("Silhouette", ImVec2(130, 30)))
            {
                ret = 4;
            }
            if (ImGui::Button("Wireframe", ImVec2(130, 30)))
            {
                ret = 5;
            }
            if (ImGui::Button("Normals", ImVec2(130, 30)))
            {
                ret = 6;
            }
            if (ImGui::Button("Explode Mesh", ImVec2(130, 30)) )
            {
                ret = 7;
            }

            ImGui::Text("Application average\n%.3f ms/frame \n(%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

            ImGui::End();

        }

        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
        
        return ret;
    };

};