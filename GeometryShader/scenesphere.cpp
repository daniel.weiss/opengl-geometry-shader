#include "scenesphere.h"

#include <iostream>
using std::cerr;
using std::endl;

#include <glm/gtc/matrix_transform.hpp>
using glm::vec3;
using glm::vec4;
using glm::mat4;

SceneSphere::SceneSphere() {
    sphere = ObjMesh::load("./models/sphere.obj");

    camera = std::unique_ptr<Camera>(new FirstPersonCamera(0.1f, glm::vec3(0.0f, 0.0f, 10.f)));
}

void SceneSphere::initScene()
{
    compileAndLinkShader();
    glClearColor(0.5f,0.5f,0.5f,1.0f);

    glEnable(GL_DEPTH_TEST);
    glPointSize(pointSize);

    float c = 1.5f;
    projection = glm::perspective(glm::radians(35.0f), (float)width / height, 0.3f, 10000.0f);

	///////////// Uniforms ////////////////////
    prog_toPoints.use();
    prog_toPoints.setUniform("Line.Width", 0.75f);
	prog_toPoints.setUniform("Line.Color", vec4(0.05f,0.0f,0.05f,1.0f));
	prog_toPoints.setUniform("Material.Kd", 0.7f, 0.7f, 0.7f);
	prog_toPoints.setUniform("Light.Position", vec4(0.0f,0.0f,0.0f, 1.0f));
	prog_toPoints.setUniform("Material.Ka", 0.2f, 0.2f, 0.2f);
	prog_toPoints.setUniform("Light.Intensity", 1.0f, 1.0f, 1.0f);
	prog_toPoints.setUniform("Material.Ks", 0.8f, 0.8f, 0.8f);
	prog_toPoints.setUniform("Material.Shininess", 100.0f);
	/////////////////////////////////////////////

    ///////////// Uniforms2 ////////////////////
    prog_passThrough.use();
    prog_passThrough.setUniform("Line.Width", 0.75f);
    prog_passThrough.setUniform("Line.Color", vec4(0.05f, 0.0f, 0.05f, 1.0f));
    prog_passThrough.setUniform("Material.Kd", 0.7f, 0.7f, 0.7f);
    prog_passThrough.setUniform("Light.Position", vec4(0.0f, 0.0f, 0.0f, 1.0f));
    prog_passThrough.setUniform("Material.Ka", 0.2f, 0.2f, 0.2f);
    prog_passThrough.setUniform("Light.Intensity", 1.0f, 1.0f, 1.0f);
    prog_passThrough.setUniform("Material.Ks", 0.8f, 0.8f, 0.8f);
    prog_passThrough.setUniform("Material.Shininess", 100.0f);
    /////////////////////////////////////////////
}


void SceneSphere::update( float t ) 
{
    //prog.setUniform("time", t);
    //prog_passThrough.setUniform("time", t);
}

void SceneSphere::render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //vec3 cameraPos(0.0f, 0.5f, 10.0f);
    //view = glm::lookAt(cameraPos,
    //                   vec3(0.0f,0.5f,0.0f),
    //                   vec3(0.0f,1.0f,0.0f));

    view = camera->getViewMatrix();

    model = mat4(1.0f);
    setMatrices();
    sphere->render();

    glFinish();
}

void SceneSphere::setMatrices()
{
    mat4 mv = view * model;
    if (points)
    {
        prog_toPoints.setUniform("ModelViewMatrix", mv);
        prog_toPoints.setUniform("NormalMatrix",
            glm::mat3(vec3(mv[0]), vec3(mv[1]), vec3(mv[2])));
        prog_toPoints.setUniform("MVP", projection * mv);
        prog_toPoints.setUniform("ViewportMatrix", viewport);
    }
    else
    {
        prog_passThrough.setUniform("ModelViewMatrix", mv);
        prog_passThrough.setUniform("NormalMatrix",
            glm::mat3(vec3(mv[0]), vec3(mv[1]), vec3(mv[2])));
        prog_passThrough.setUniform("MVP", projection * mv);
        prog_passThrough.setUniform("ViewportMatrix", viewport);
    }

}

void SceneSphere::resize(int w, int h)
{
    glViewport(0,0,w,h);

    float w2 = w / 2.0f;
    float h2 = h / 2.0f;
    viewport = mat4( vec4(w2,0.0f,0.0f,0.0f),
                     vec4(0.0f,h2,0.0f,0.0f),
                     vec4(0.0f,0.0f,1.0f,0.0f),
                     vec4(w2+0, h2+0, 0.0f, 1.0f));
    projection = glm::perspective(glm::radians(35.0f), (float)w / h, 0.3f, 10000.0f);

}

void SceneSphere::acceptUserInput(int mode, int x, int y)
{

    if (mode == GLFW_CURSOR)
    {
        camera->updateCameraFromMouseInput(x, y);
    }

    camera->updateCameraFromKeryboardInput(mode);

}

int SceneSphere::renderGUI()
{
    int ret = 0;
    //Start the Dear ImGui frame
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();

    ImGui::NewFrame();
    {
        ImGui::Begin("Navigation");

        if (ImGui::Button("Back", ImVec2(130, 30)))
        {
            ret = -1;
        }
        //ImGui::Checkbox("Animating", &m_animate);

        if (ImGui::Button("to Points"))
        {
            if (points)
                prog_passThrough.use();
            else
                prog_toPoints.use();

            points = !points;
        }

        if (ImGui::SliderInt("Point-size", &pointSize, 1, 100))
            glPointSize(pointSize);

        ImGui::Text("Application average\n%.3f ms/frame \n(%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);

        ImGui::End();
    }

    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    return ret;
}

void SceneSphere::compileAndLinkShader()
{
	try {
		prog_toPoints.compileShader("shader/sphere.vert",GLSLShader::VERTEX);
		prog_toPoints.compileShader("shader/sphere.frag",GLSLShader::FRAGMENT);
		prog_toPoints.compileShader("shader/spherepoints.geom", GLSLShader::GEOMETRY);
		prog_toPoints.link();

        prog_passThrough.compileShader("shader/sphere.vert", GLSLShader::VERTEX);
        prog_passThrough.compileShader("shader/sphere.frag", GLSLShader::FRAGMENT);
        prog_passThrough.compileShader("shader/sphere.geom",GLSLShader::GEOMETRY);
        prog_passThrough.link();
        prog_passThrough.use();

    } catch(GLSLProgramException &e ) {
    	cerr << e.what() << endl;
 		exit( EXIT_FAILURE );
    }
}
