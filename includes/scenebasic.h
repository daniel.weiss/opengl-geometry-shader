#ifndef SCENEBASIC_H
#define SCENEBASIC_H

#include "scene.h"
#include "Object.h"

#include "cookbookogl.h"
#include <string>

class SceneBasic : public Scene
{
public:
    SceneBasic();

    void initScene();
    void update( float t );
    void render();
    void resize(int, int);

private:
    //GLuint vaoHandle;
    GLuint programHandle;

    std::vector<Object> scene_movable;
    std::vector<Object> scene_immovable;

    GLuint vertices = 0;

    glm::mat4 view;
    glm::mat4 projection;
    glm::mat4 model;
    glm::mat4 MVP;

    void linkMe(GLint vertShader, GLint fragShader);
    void compileShaderProgram();
    void writeShaderBinary();
    void loadShaderBinary(GLint);
    void loadSpirvShader();

    void setupCamera();
    void setupPlane();
    void populateScene();
    void singleGrass();

    std::string getShaderInfoLog(GLuint shader);
    std::string getProgramInfoLog(GLuint program);

    int currentIndex = 0;
    int currentIndexOffset1 = 60;
    int currentIndexOffset2 = 120;

    std::vector<float> rotation_degrees = 
    {
        45.5857,
        45.2497,
        45.0727,
        45.0349,
        45.1097,
        45.2649,
        45.4647,
        45.671 ,
        45.8454,
        45.951 ,
        45.9543,
        45.8268,
        45.546 ,
        45.0972,
        44.474 ,
        43.6788,
        42.723 ,
        41.6269,
        40.4185,
        39.1334,
        37.8128,
        36.5021,
        35.2496,
        34.1041,
        33.113 ,
        32.3208,
        31.7669,
        31.484 ,
        31.4969,
        31.8212,
        32.4624,
        33.4159,
        34.6665,
        36.1892,
        37.9495,
        39.9049,
        42.0064,
        44.1995,
        46.4268,
        48.6297,
        50.7505,
        52.7341,
        54.5304,
        56.0956,
        57.394 ,
        58.3988,
        59.0931,
        59.4706,
        59.535 ,
        59.3004,
        58.79  ,
        58.0352,
        57.0746,
        55.9518,
        54.7139,
        53.4096,
        52.0874,
        50.7933,
        49.5694,
        48.4521,
        47.4711,
        46.6479,
        45.9954,
        45.5174,
        45.2092,
        45.0573,
        45.0404,
        45.1311,
        45.2964,
        45.5   ,
        45.7036,
        45.8689,
        45.9596,
        45.9427,
        45.7908,
        45.4826,
        45.0046,
        44.3521,
        43.5289,
        42.5479,
        41.4306,
        40.2067,
        38.9126,
        37.5904,
        36.2861,
        35.0482,
        33.9254,
        32.9648,
        32.21  ,
        31.6996,
        31.465 ,
        31.5294,
        31.9069,
        32.6012,
        33.606 ,
        34.9044,
        36.4696,
        38.2659,
        40.2495,
        42.3703,
        44.5732,
        46.8005,
        48.9936,
        51.0951,
        53.0505,
        54.8108,
        56.3335,
        57.5841,
        58.5376,
        59.1788,
        59.5031,
        59.516 ,
        59.2331,
        58.6792,
        57.887 ,
        56.8959,
        55.7504,
        54.4979,
        53.1872,
        51.8666,
        50.5815,
        49.3731,
        48.277 ,
        47.3212,
        46.526 ,
        45.9028,
        45.454 ,
        45.1732,
        45.0457,
        45.049 ,
        45.1546,
        45.329 ,
        45.5353,
        45.7351,
        45.8903,
        45.9651,
        45.9273,
        45.7503,
        45.4143,
        44.9071,
        44.2254,
        43.3745,
        42.3688,
        41.2314,
        39.993 ,
        38.691 ,
        37.3685,
        36.072 ,
        34.8501,
        33.7513,
        32.8223,
        32.1062,
        31.6401,
        31.4544,
        31.5708,
        32.0015,
        32.7488,
        33.8044,
        35.1498,
        36.7566,
        38.5876,
        40.598 ,
        42.7365,
        44.9476,
        47.1733,
        49.3549,
        51.4355,
        53.3614,
        55.0845,
        56.5636,
        57.7657,
        58.6675,
        59.2555,
        59.5267,
        59.4885,
        59.1581,
        58.5616,
        57.7331,
        56.7128,
        55.5459,
        54.2802,
        52.9646,
        51.6468,
        50.3719,
        49.1801,
        48.1058,
        47.1758,
        46.409 ,
        45.8152
    };

};

#endif // SCENEBASIC_H
