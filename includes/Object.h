#pragma once

#include <vector>
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glutils.h"

# define M_PI           3.14159265358979323846

class Object
{
public:
	Object() = delete;
	explicit Object(const std::vector<glm::vec3>, const std::vector<glm::vec3>);

	void setProjection(const glm::mat4);
	void setView(const glm::mat4);

	void update(GLuint programHandle);

	void render();

	void setOriginalRotation(const glm::mat4&);

	// rotate scale translate
	void rotate(float, float, float);

	void translate(float, float, float);

	void scale(float);

private:
	GLuint vaoHandle;

	std::vector<glm::vec3> data;
	std::vector<glm::vec3> color;

	glm::mat4 MVP;
	glm::mat4 view;
	glm::mat4 projection;
	
	glm::mat4 model;
	glm::mat4 translateMat;
	glm::mat4 rotateMat;
	glm::mat4 originalRotation;
	glm::mat4 scaleMat;

	void recalculateModel();

};

