#include "cookbookogl.h"

#include "scenePointToCube.h"
#include "scenepointsprite.h"
#include "sceneshadewire.h"
#include "scenesilhouette.h"
#include "scenenormals.h"
#include "sceneexplosion.h"
#include "scenesphere.h"
#include "SwapScene.h"
#include <GLFW/glfw3.h>

#include "glutils.h"

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

#include <map>
#include <string>
#include <fstream>
#include <iostream>
#include <memory>
#include <map>
//#include "Camera.h"

class SceneRunner {
private:
    GLFWwindow * window;
    int fbw, fbh;
	bool debug;           // Set true to enable debug messages

    Scene* scene = nullptr;

    int mouse_x;
    int mouse_y;

public:
    SceneRunner(const std::string& windowTitle, int width = WIN_WIDTH, int height = WIN_HEIGHT, int samples = 0) : debug(true), mouse_x(0), mouse_y(0) {
        // Initialize GLFW
        if( !glfwInit() ) exit( EXIT_FAILURE );

#ifdef __APPLE__
        // Select OpenGL 4.1
        glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 4 );
        glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 1 );
#else
        // Select OpenGL 4.6
        glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 4 );
        glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 6 );
#endif
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
        glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);

        if(debug)
			glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
        if(samples > 0) {
            glfwWindowHint(GLFW_SAMPLES, samples);
        }

        // Open the window
        window = glfwCreateWindow( WIN_WIDTH, WIN_HEIGHT, windowTitle.c_str(), NULL, NULL );
        if( ! window ) {
			std::cerr << "Unable to create OpenGL context." << std::endl;
            glfwTerminate();
            exit( EXIT_FAILURE );
        }
        glfwMakeContextCurrent(window);

        // Get framebuffer size
        glfwGetFramebufferSize(window, &fbw, &fbh);

        // Load the OpenGL functions.
        if(!gladLoadGL()) { exit(-1); }

        GLUtils::dumpGLInfo();

        // Initialization
        glClearColor(0.5f,0.5f,0.5f,1.0f);
#ifndef __APPLE__
		if (debug) {
			glDebugMessageCallback(GLUtils::debugCallback, nullptr);
            glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_FALSE);
			glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_MEDIUM, 0, NULL, GL_TRUE);
            glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_HIGH, 0, NULL, GL_TRUE);
			glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_MARKER, 0,
				GL_DEBUG_SEVERITY_NOTIFICATION, -1, "Start debugging");
		}
#endif

        // Initialize ImGUI

        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO& io = ImGui::GetIO(); (void)io;
        //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
        //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

        // Setup Dear ImGui style
        ImGui::StyleColorsDark();
        //ImGui::StyleColorsClassic();

        // Setup Platform/Renderer backends
        ImGui_ImplGlfw_InitForOpenGL(window, true);
        const char* glsl_version = "#version 460";
        ImGui_ImplOpenGL3_Init(glsl_version);

    }

    int run(std::unique_ptr<Scene> scene) {        
        // Enter the main loop
        mainLoop(window, std::move(scene));

#ifndef __APPLE__
		if( debug )
			glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_MARKER, 1,
				GL_DEBUG_SEVERITY_NOTIFICATION, -1, "End debug");
#endif

		// Close window and terminate GLFW
		glfwTerminate();

        // Exit program
        return EXIT_SUCCESS;
    }

    int run() {
        // Enter the main loop
        mainLoop(window);

#ifndef __APPLE__
        if (debug)
            glDebugMessageInsert(GL_DEBUG_SOURCE_APPLICATION, GL_DEBUG_TYPE_MARKER, 1,
                GL_DEBUG_SEVERITY_NOTIFICATION, -1, "End debug");
#endif

        // Close window and terminate GLFW
        glfwTerminate();

        // Exit program
        return EXIT_SUCCESS;
    }

    static std::string parseCLArgs(int argc, char ** argv, std::map<std::string, std::string> & sceneData) {
        if( argc < 2 ) {
            printHelpInfo(argv[0], sceneData);
            exit(EXIT_FAILURE);
        }

        std::string recipeName = argv[1];
        auto it = sceneData.find(recipeName);
        if( it == sceneData.end() ) {
            printf("Unknown recipe: %s\n\n", recipeName.c_str());
            printHelpInfo(argv[0], sceneData);
            exit(EXIT_FAILURE);
        }

        return recipeName;
    }

private:
    static void printHelpInfo(const char * exeFile,  std::map<std::string, std::string> & sceneData) {
        printf("Usage: %s recipe-name\n\n", exeFile);
        printf("Recipe names: \n");
        for( auto it : sceneData ) {
            printf("  %11s : %s\n", it.first.c_str(), it.second.c_str());
        }
    }

    void mainLoop(GLFWwindow * window, std::unique_ptr<Scene> scene) {
        
        //c = Camera(glm::vec3(0.0f, 0.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));

        scene->setDimensions(fbw, fbh);
        scene->initScene();
        scene->resize(fbw, fbh);

        while( ! glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE) ) {
            GLUtils::checkForOpenGLError(__FILE__,__LINE__);
			
            glfwGetFramebufferSize(window, &fbw, &fbh);
            scene->resize(fbw, fbh);
            scene->update(float(glfwGetTime()));
            scene->render();
            
            swapScene(scene->renderGUI());

            glfwSwapBuffers(window);

            glfwPollEvents();
			
            processUserInput(scene.get());
            
        }

    }

    void processUserInput(Scene *scene)
    {
           
        if (glfwGetKey(window, GLFW_KEY_W))
        {
            scene->acceptUserInput(GLFW_KEY_W);
        }
        if (glfwGetKey(window, GLFW_KEY_A))
        {
            scene->acceptUserInput(GLFW_KEY_A);
        }
        if (glfwGetKey(window, GLFW_KEY_S))
        {
            scene->acceptUserInput(GLFW_KEY_S);
        }
        if (glfwGetKey(window, GLFW_KEY_D))
        {
            scene->acceptUserInput(GLFW_KEY_D);
        }
        if (glfwGetKey(window, GLFW_KEY_E))
        {
            scene->acceptUserInput(GLFW_KEY_E);
        }
        if (glfwGetKey(window, GLFW_KEY_R))
        {
            scene->acceptUserInput(GLFW_KEY_R);
        }
        if (glfwGetKey(window, GLFW_KEY_SPACE))
        {
            scene->acceptUserInput(GLFW_KEY_SPACE);
        }
        if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL))
        {
            scene->acceptUserInput(GLFW_KEY_LEFT_CONTROL);
        }

        if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
        {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
            double x, y;
            glfwGetCursorPos(window, &x, &y);
            //std::cout << x << " : " << y << std::endl;

            int delta_x = x - mouse_x;
            int delta_y = y - mouse_y;

            scene->acceptUserInput(GLFW_CURSOR, delta_x, delta_y);

            mouse_x = x;
            mouse_y = y;

        }
        else
        {
            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
            double x, y;
            glfwGetCursorPos(window, &x, &y);
            mouse_x = x;
            mouse_y = y;

        }
    }

    void mainLoop(GLFWwindow* window) {

        //c = Camera(glm::vec3(0.0f, 0.0f, 2.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f));

        scene = new SwapScene();

        scene->setDimensions(fbw, fbh);
        scene->initScene();
        scene->resize(fbw, fbh);

        while (!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE)) {
            GLUtils::checkForOpenGLError(__FILE__, __LINE__);

            glfwGetFramebufferSize(window, &fbw, &fbh);
            scene->resize(fbw, fbh);
            scene->update(float(glfwGetTime()));
            scene->render();

            swapScene(scene->renderGUI());

            glfwSwapBuffers(window);

            glfwPollEvents();
            
            processUserInput(scene);

        }

    }

    void swapScene(int index)
    {
        if (index != 0)
        {
            delete scene;

            switch (index)
            {
             case 1: 
                 scene = new SceneSphere();
                 break;
             case 2: 
                 scene = new ScenePointToCube();
                 break;
             case 3: 
                 scene = new ScenePointSprite();
                 break;
             case 4: 
                 scene = new SceneSilhouette();
                 break;
             case 5: 
                 scene = new SceneShadeWire();
                 break;
             case 6: 
                 scene = new SceneNormals();
                 break;
             case 7: 
                 scene = new SceneExplosion();
                 break;
             case -1: 
                 scene = new SwapScene();
                 break;
            }

            scene->setDimensions(fbw, fbh);
            scene->initScene();
            scene->resize(fbw, fbh);
        }

    }
};
